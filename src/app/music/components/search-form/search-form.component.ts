import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidatorFn, AbstractControl, ValidationErrors, AsyncValidatorFn, AsyncValidator, FormBuilder } from '@angular/forms';
import { distinctUntilChanged, filter, debounceTime, map, combineLatest, withLatestFrom } from 'rxjs/operators';
import { Observable, Observer } from 'rxjs';
// const trim = o => o.pipe(map(q => q.trim()));

const censor = (badword) => (control: AbstractControl): ValidationErrors | null => {
  const hasError = (control.value as string).includes(badword)
  if (hasError) {
    return {
      'censor': { badword }
    }
  } else {
    return null
  }
}
const asynCensor: AsyncValidatorFn = (control: AbstractControl): Observable<ValidationErrors | null> => {
  // return this.http.get('./validate').pipe(map(resp=>resp.errors))

  return Observable.create((observer: Observer<ValidationErrors | null>) => {
    const handler = setTimeout(() => {
      const error = censor('batman')(control)
      observer.next(error)
      observer.complete()
    }, 2000)

    return /* onUnsubscribe */ () => {
      clearTimeout(handler)
    }
  })
}

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {

  searchForm = this.fb.group({
    'query': ['', [
      Validators.required,
      Validators.minLength(3),
    ], [
        asynCensor
      ]],
    'type': ['album']
  })

  @Input()
  set query(query) {
    this.searchForm.get('query').setValue(query, {
      emitEvent: false
    })
  }

  constructor(private fb: FormBuilder) {


    const field = this.searchForm.get('query')

    const searchChanges = field.statusChanges.pipe(
      filter(status => status == "VALID"),
      withLatestFrom(field.valueChanges, (status, value) => {
        return value
      })
    )

    searchChanges.pipe(
      map(q => q.trim()),
      debounceTime(400),
      distinctUntilChanged(),
      filter(q => q.length >= 3),
    )
      .subscribe(query => {
        this.search(query)
      })
  }

  ngOnInit() {
  }

  @Output()
  searchChange = new EventEmitter()

  search(query: string) {
    this.searchChange.emit(query)
  }

}
