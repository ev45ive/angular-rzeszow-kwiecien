import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MusicRoutingModule } from './music-routing.module';
import { MusicSearchComponent } from './views/music-search/music-search.component';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { SearchResultsComponent } from './components/search-results/search-results.component';
import { AlbumCardComponent } from './components/album-card/album-card.component';
import { environment } from 'src/environments/environment';
import { MUSIC_API_URL, MusicSearchService } from './services/music-search.service';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [MusicSearchComponent, SearchFormComponent, SearchResultsComponent, AlbumCardComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    MusicRoutingModule,
    ReactiveFormsModule
  ],
  exports: [MusicSearchComponent],
  providers: [
    {
      provide: MUSIC_API_URL,
      useValue: environment.api_url
    },
    // {
    //   provide: MusicSearchService,
    //   useFactory: function (url) {
    //     return new MusicSearchService(url)
    //   },
    //   deps: [MUSIC_API_URL]
    // },
    // {
    //   provide: AbstractSearchService,
    //   useClass: SpotifySearchService,
    //   // deps: [MUSIC_API_URL]
    // },
    // {
    //   provide: MusicSearchService, 
    //   useClass: MusicSearchService
    // },
    // MusicSearchService
  ]
})
export class MusicModule { }
