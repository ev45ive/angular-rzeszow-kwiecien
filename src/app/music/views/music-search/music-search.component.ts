import { Component, OnInit } from '@angular/core';
import { Album } from 'src/app/models/album';
import { MusicSearchService } from '../../services/music-search.service';
import { tap, share } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-music-search',
  templateUrl: './music-search.component.html',
  styleUrls: ['./music-search.component.scss']
})
export class MusicSearchComponent implements OnInit {

  queryChanges = this.service.queryChanges

  albumChanges = this.service.getAlbums()
    .pipe(
      tap(() => this.albums),
      // multicast(() => new Subject()),
      // refCount(),
      share()
    )

  albums: Album[]

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: MusicSearchService
  ) { }

  search(query: string) {
    this.service.search(query)

    this.router.navigate([], {
      queryParams: {
        query
      },
      relativeTo: this.route,
      replaceUrl: true
    })
  }

  ngOnInit() {
    this.route.queryParamMap.subscribe(paramMap => {
      const query = paramMap.get('query')
      if (query) {
        this.search(query)
      }
    })

    // const query = this.route.snapshot.queryParamMap.get('query')
    // if (query) {
    //   this.search(query)
    // }
  }


}
