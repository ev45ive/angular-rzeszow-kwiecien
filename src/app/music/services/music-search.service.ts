import { Injectable, Inject, InjectionToken, EventEmitter } from '@angular/core';
import { Album, AlbumsResponse } from 'src/app/models/album';
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { AuthService } from 'src/app/security/auth.service';
import { map, catchError, concat, startWith, mergeMap, concatMap, switchMap } from 'rxjs/operators'
import { of, empty, throwError, Subject, ReplaySubject, BehaviorSubject } from 'rxjs';

export const MUSIC_API_URL =
  new InjectionToken('Api url for music search')

@Injectable({
  // providedIn: MusicModule
  providedIn: 'root'
})
export class MusicSearchService {

  constructor(
    private http: HttpClient,
    private auth: AuthService,
    @Inject(MUSIC_API_URL)
    private api_url: string) {

    (window as any).albumsChanges = this.albumsChanges


    this.queryChanges
      .pipe(
        map(query => ({
          type: 'album',
          q: query
        })),
        // mergeMap / concatMap / switchMap
        switchMap(params => this.http.get<AlbumsResponse>(this.api_url, {
          headers: {
            Authorization: `Bearer ${this.auth.getToken()}`
          }, 
          params
        })),
        map(resp => resp.albums.items),
        catchError((err: HttpErrorResponse) => {
          if (err.status == 401) {
            this.auth.authorize()
          }
          return empty()
        }))
      .subscribe((albums) => {
        this.albumsChanges.next(albums)
      })

  }

  search(query: string) {
    this.queryChanges.next(query)
  }

  getAlbums() {
    return this.albumsChanges
  }

  albums: Album[] = [
    {
      id: '123',
      name: 'Test 12',
      artists: [],
      images: [
        {

          height: 300,
          width: 300,
          url: 'http://placekitten.com/300/300'
        }
      ]
    },
    {
      id: '123',
      name: 'Test',
      artists: [],
      images: [
        {

          height: 300,
          width: 300,
          url: 'http://placekitten.com/300/300'
        }
      ]
    },
    {
      id: '123',
      name: 'Test',
      artists: [],
      images: [
        {

          height: 300,
          width: 300,
          url: 'http://placekitten.com/300/300'
        }
      ]
    },
    {
      id: '123',
      name: 'Test',
      artists: [],
      images: [
        {

          height: 300,
          width: 300,
          url: 'http://placekitten.com/300/300'
        }
      ]
    },
    {
      id: '123',
      name: 'Test',
      artists: [],
      images: [
        {

          height: 300,
          width: 300,
          url: 'http://placekitten.com/300/300'
        }
      ]
    },
    {
      id: '123',
      name: 'Test',
      artists: [],
      images: [
        {

          height: 300,
          width: 300,
          url: 'http://placekitten.com/300/300'
        }
      ]
    },
    {
      id: '123',
      name: 'Test',
      artists: [],
      images: [
        {

          height: 300,
          width: 300,
          url: 'http://placekitten.com/300/300'
        }
      ]
    },
  ]

  albumsChanges = new BehaviorSubject<Album[]>(this.albums)
  queryChanges = new BehaviorSubject<string>('batman')
  // queryChanges = new ReplaySubject<string>(1)


}
