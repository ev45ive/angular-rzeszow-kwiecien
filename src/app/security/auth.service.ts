import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

//https://angular.io/api/common/http/HttpClientXsrfModule

export class AuthConfig {
  auth_url: string
  client_id: string
  response_type: string
  redirect_uri: string
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private config: AuthConfig) {
    const tokenJSON = sessionStorage.getItem('token')
    if (tokenJSON) {
      this.token = JSON.parse(tokenJSON)
    }

    if (location.hash && !this.token) {
      const params = new HttpParams({ fromString: location.hash })
      this.token = params.get('#access_token')
      if (this.token) {
        location.hash = ''
        sessionStorage.setItem('token', JSON.stringify(this.token))
      }
    }
  }

  authorize() {
    const { response_type, redirect_uri, client_id, auth_url } = this.config

    const params = new HttpParams({
      fromObject: { response_type, redirect_uri, client_id }
    })
    sessionStorage.removeItem('token')
    location.href = (`${auth_url}?${params.toString()}&placki=123`)
  }

  private token: null | string = null

  getToken() {
    if (!this.token) {
      this.authorize()
    }
    return this.token
  }
}
