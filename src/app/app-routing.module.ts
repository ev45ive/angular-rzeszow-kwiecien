import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlaylistsViewComponent } from './playlists/views/playlists-view/playlists-view.component';
import { MusicSearchComponent } from './music/views/music-search/music-search.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'playlists', pathMatch: 'full'
  },
  {
    path: 'playlists',
    component: PlaylistsViewComponent
  },
  {
    path: 'playlists/:id',
    component: PlaylistsViewComponent
  },
  {
    path: 'music',
    component: MusicSearchComponent
  },
  {
    path: '**',
    // component: PageNotFOundViewComponent,
    redirectTo: 'playlists', pathMatch: 'full',
    // matcher(){}
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    enableTracing: true,
    // useHash: true
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
