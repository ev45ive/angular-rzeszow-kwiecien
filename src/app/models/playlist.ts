export interface Playlist {
    // type: 'Playlist'
    id: number
    name: string
    favorite: boolean
    /**
     * HEX Color 
     */
    color: string
    tracks?: Track[]
    // tracks: Array<Track>
}

export interface Track {
    id: number
    name: string
}