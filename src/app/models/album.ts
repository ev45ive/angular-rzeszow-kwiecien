// http://www.jsontots.com/
// https://developer.spotify.com/documentation/web-api/reference/albums/get-several-albums/
export interface Entity {
    id: string;
    name: string
}
export interface Album extends Entity {
    images: Image[];
    artists: Artist[];
}

export interface Image {
    height: number;
    url: string;
    width: number;
}

export interface Artist extends Entity { }

export interface PageObject<T> {
    items: T[]
}

export interface AlbumsResponse {
    albums: PageObject<Album>
}

// export interface Entity {
//     id: string;
//     name: string
// }

// export class Entity {
//     id: string;
//     name: string
//     DomSharedStylesHost(){}
// }

// const x =  new Entity(dataformserver.initialdata1, dataformserver.data2)
// const x =  Entity.fromJson(dataformserver)