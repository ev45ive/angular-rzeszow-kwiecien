import { Component, OnInit, ViewEncapsulation, Input, EventEmitter, Output } from '@angular/core';
import { Playlist } from 'src/app/models/playlist';
import { NgForOfContext } from '@angular/common';

@Component({
  selector: 'app-items-list',
  templateUrl: './items-list.component.html',
  styleUrls: ['./items-list.component.scss'],
  // inputs:['playlists:items']
})
export class ItemsListComponent implements OnInit {

  @Input('items')
  playlists: Playlist[] = []

  @Input()
  selected: Playlist

  @Output()
  selectedChange = new EventEmitter<Playlist>()

  select(selected: Playlist) {
    this.selectedChange.emit(selected)
    this.selected = selected
  }

  constructor() { }

  ngOnInit() {
  }

  indexFn(index, item) {
    return item.id
  }

}
