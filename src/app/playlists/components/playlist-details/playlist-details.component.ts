import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Playlist } from 'src/app/models/playlist';
import { NgForm } from '@angular/forms';

enum MODES {
  edit, show
}

@Component({
  selector: 'app-playlist-details',
  templateUrl: './playlist-details.component.html',
  styleUrls: ['./playlist-details.component.scss']
})
export class PlaylistDetailsComponent implements OnInit {

  @Input()
  playlist: Playlist

  mode: 'show' | 'edit' = 'show'

  constructor() {
  }

  ngOnInit() {
  }

  edit() {
    this.mode = 'edit'
  }

  cancel() {
    this.mode = 'show'
  }

  @Output()
  playlistSave = new EventEmitter<Playlist>();

  save(form: NgForm) {
    // Object.assign({}, this.playlist, form.value)

    const draft = {
      ...this.playlist,
      ...form.value
    }

    this.playlistSave.emit(draft)
    this.mode = 'show'
  }


}
