import { Component, OnInit } from '@angular/core';
import { Playlist } from 'src/app/models/playlist';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-playlists-view',
  templateUrl: './playlists-view.component.html',
  styleUrls: ['./playlists-view.component.scss']
})
export class PlaylistsViewComponent implements OnInit {

  playlists: Playlist[] = [
    {
      id: 123,
      name: 'Angular Hits',
      favorite: true,
      color: '#ff00ff'
    }, {
      id: 234,
      name: 'Angular TOP20',
      favorite: false,
      color: '#ffff00'
    }, {
      id: 345,
      name: 'Best of Angular',
      favorite: true,
      color: '#00ffff'
    },
  ]

  selected: Playlist = this.playlists[1]

  save(draft: Playlist) {
    const index = this.playlists.findIndex(//
      p => p.id == draft.id
    )
    if (index !== -1) {
      this.playlists.splice(index, 1, draft)
      this.selected = draft
    }
  }

  select(playlist: Playlist) {
    this.router.navigate(['/playlists', playlist.id], {
      replaceUrl: true
    })
  }

  constructor(
    private router: Router,
    private route: ActivatedRoute) {
    this.route.paramMap.subscribe(paramMap => {
      const id = paramMap.get('id')

      const playlist = this.playlists.find(p => p.id == parseInt(id))

      if (playlist) {
        this.selected = playlist
      }
    })
  }

  ngOnInit() {
  }

}
