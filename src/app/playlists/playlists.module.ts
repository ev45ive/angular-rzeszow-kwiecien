import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlaylistsRoutingModule } from './playlists-routing.module';
import { PlaylistsViewComponent } from './views/playlists-view/playlists-view.component';
import { ItemsListComponent } from './components/items-list/items-list.component';
import { ListItemComponent } from './components/list-item/list-item.component';
import { PlaylistDetailsComponent } from './components/playlist-details/playlist-details.component';

// ng g c playlists/views/playlists-view --export true 
// ===
// ng g c playlists/components/items-list
// ng g c playlists/components/list-item
// ng g c playlists/components/playlist-details
import { FormsModule } from '@angular/forms'

@NgModule({
  declarations: [
    PlaylistsViewComponent, 
    ItemsListComponent, 
    ListItemComponent, 
    PlaylistDetailsComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    PlaylistsRoutingModule
  ],
  exports:[
    PlaylistsViewComponent
  ]
})
export class PlaylistsModule { }
